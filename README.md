# 2180607724_Huynh My Nhat Mai


#Nguyễn Nhân Quỳnh Như

| Title               | Teacher changes students's score                    |
|:-------------------:|:-----------------------------------------------|
| Value Statement     | As a teacher, I want to change student's score | 
| Acceptence Criter   | Acceptence Criterion: If student doesn't take the exam, they get 0 points;<br>if their score is wrong then correct it.|
| Definition Of Done  | - Unit Tests Passed<br>- Acceptence Criteria Met<br>- Code Reviewed<br>- Functional Tests Passed<br>- Non-Functional Requirements Met<br>- Product Owner Accepts User Story|
| Owner               | Nguyen Nhan Quynh Nhu                          | 
