# 2180607724_Huynh My Nhat Mai


#Nguyễn Hoang Chương
| Title:                | Manager arrange Student with increasing avgScore                                                                                               |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement:      | As a Manager,<br>I want to arrange student with increasing avgScore<br>so I can easily find the student with the highest avgScore.             |
| Acceptance Criterion: | Acceptance Criterion 1:<br>When managing chooses to select the sorting option,<br>the student avgScore will be sorted ascending or descending. |
| Definition of Done:   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story                                                                                    |
| Owner               | Nguyen Hoang Chuong                          | 
