# HuynhMyNhatMai.it
##Họ tên : Huỳnh Mỹ Nhật Mai
##Mssv : 2180607724
##Lớp : 21DTHD4

|Order						| Content									|
| :---:						| :---: 									|
|**Title**					| Search student's information base on ID 	|
|**Value statement**		| As a teacher or student , i want search student's infomation base on ID |
|**Acceptence Criteria**  	| When teacher or student press "Search" after entering the student ID , the student information will be display |
|**Definition of done**		| Unit tests passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional tests passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story	|
|**Owner**					| Nhat Mai 									|

##Nguyễn Nhân Quỳnh Như
##MSSV:2180608719
| Title               | Teacher changes students's score                    |
|:-------------------:|:-----------------------------------------------|
| Value Statement     | As a teacher, I want to change student's score | 
| Acceptence Criter   | Acceptence Criterion: If student doesn't take the exam, they get 0 points;<br>if their score is wrong then correct it.|
| Definition Of Done  | - Unit Tests Passed<br>- Acceptence Criteria Met<br>- Code Reviewed<br>- Functional Tests Passed<br>- Non-Functional Requirements Met<br>- Product Owner Accepts User Story|
| Owner               | Nguyen Nhan Quynh Nhu                          | 

##Nguyễn Hoàng Chương
##MSSV:2180607334
| Title:                | Manager arrange Student with increasing avgScore                                                                                               |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement:      | As a Manager,<br>I want to arrange student with increasing avgScore<br>so I can easily find the student with the highest avgScore.             |
| Acceptance Criterion: | Acceptance Criterion 1:<br>When managing chooses to select the sorting option,<br>the student avgScore will be sorted ascending or descending. |
| Definition of Done:   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story                                                                                    |
| Owner               | Nguyen Hoang Chuong                          | 

##Đặng Văn Đạt
##MSSV:2180607411
| Title                | Teacher insert student to the list             |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | If there is new,as a teacher,I want to add them in list.|
| Acceptance Criterion | Acceptance Criterion : Add new student information to the list|
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Dang Van Dat     |

##Lương Thiện Hưng
##MSSV:2180607587
| Title                |  Teachers delete students based on ID       |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a teacher, I want to remove student information from the system based on ID to search.|
| Acceptance Criterion | Acceptence Criterion 1:Students can be deleted from the system when ID is found<br>Acceptence Criterion 2: Display a notification if the student has been successfully deleted from the system  |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Luong Thien Hung                                   | 




